require 'optparse'

module Documentify
  VERSION = '0.1.1'

  def self.wrap(content)
<<END_DOC
<html>
  <head></head>
  <body>
    #{content}
  </body>
</html>
END_DOC
  end

  def self.from_cli(argv)
    opt = OptionParser.new do |opts|
      opts.banner = "Usage: documentify [options] [content.html]"
      opts.on_tail '-h', '--help', 'Show this message' do
        puts opts
        exit
      end
    end

    begin
      opt.parse! argv
    rescue OptionParser::InvalidOption => e
      $stderr.puts e
      $stderr.puts opt
      exit
    end

    if argv.empty? # from stdin
      input = $stdin.read
    else
      input = File.read(ARGV.shift)
    end

    print self.wrap(input)
  end
end