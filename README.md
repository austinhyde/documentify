**Documentify is incomplete and a work in progress. Nothing should be expected to work yet.**

# Documentify

Documentify is a simple utility that wraps some content in an HTML document, regardless of the type of content (although HTML is only logical!).

## Installation

With RubyGems,

	$ gem install documentify

## Usage

Documentify can take input from stdin:

	$ echo '<h1>Header!</h1><p>Paragraph!</p>' | documentify
	<html>
	<head></head>
	<body>
	<h1>Header!</h1><p>Paragraph!</p>
	</body>
	</html>

Or a named file:

	$ documentify sample.html
	<html>
	<head></head>
	<body>
	<h1>Header!</h1><p>Paragraph!</p>
	</body>
	</html>

## Suggested Use

Many tools (I'm looking at you, Markdown!) only output fragments of HTML documents, which isn't really useful, so Documentify can 
help alleviate that pain:

	$ markdown README.md | documentify | browser

(Using the wonderful [browser][browser] utility to pipe to a browser)

## Roadmap

Eventually I would like to add things like custom titles, escaping, pretty printing, pre/post HTML, and more:

	$ markdown sample.html | documentify --doctype=html5 --title='Sample Page' --pre='<em>Something before the content</em><pre>' --post='</pre><em>Something after the content</em>' --escape
	<!DOCTYPE html>
	<html>
		<head>
			<title>Sample Page</title>
		</head>
		<body>
			<em>Something before the content</em>
			<pre>
	&lt;h1&gt;Header!&lt;/h1&gt;&lt;p&gt;Paragraph!&lt;/p&gt;
			</pre>
			<em>Something after the content</em>
		</body>
	</html>

Of course, proper documentation and unit tests are on their way.



[browser]: https://gist.github.com/318247