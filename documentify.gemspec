require 'rubygems'
require './lib/documentify'
spec = Gem::Specification.new do |spec|
  spec.name = 'documentify'
  spec.summary = 'Wraps things in an HTML document'
  spec.description = 'Documentify takes input from a file or stdin and wraps it in a proper HTML document'
  spec.author = 'Austin Hyde'
  spec.email = 'austin109@gmail.com'
  spec.homepage = 'http://bitbucket.org/austinhyde/documentify'
  spec.files = Dir['lib/*.rb'] + Dir['bin/*']
  spec.bindir = 'bin'
  spec.executables = ['documentify']
  spec.require_path = 'lib'
  spec.version = Documentify::VERSION
end